---
title: Gallery | WPAFW
permalink: "/gallery"
layout: page
banner: 🌮 Gallery
banner-color: green
nav: false
---

# 2022 Gallery

{% for image in site.static_files %}
{% if image.path contains 'img/2022' %}
<figure class="image">
<a href="{{site.baseurl}}{{image.path}}" target="_blank">
<img src="{{ site.baseurl }}{{ image.path }}" alt="image" />
</a>
</figure>
{% endif %}
{% endfor %}
