---
title: Featured Artists | WPAFW
permalink: "/artists"
layout: page
banner: ‍🎨 Featured Artists
banner-color: blue
nav: true
nav-title: Artists
nav-order: 7
---

# Artists (2022)

## 🎨 Featured Artist(s)


<div class="columns is-mobile">
<div class="column is-half is-offset-one-quarter">
<div class="card">
<div class="card-image">
<figure class="image is-square">
<img src="{{'/assets/img/artists/rel.png' | absolute_url}}" alt="Rel's Avatar.">
</figure>
</div>
<div class="card-content">
<div class="media">
<div class="media-content">
<p class="title is-4">Rel</p>
<p class="subtitle is-6">[@Relosaurus](https://twitter.com/Relosaurus)</p>
</div>
</div>

<div class="content">

<p>Rel is an artist currently living in the Hudson Valley area of New York.
They lived in Pittsburgh from 2015-2020, when they first volunteered and then 
became staff for WPAFW. They were first introduced to the furry community when 
they were 13, and they immediately fell in love with the big fluffy critters. 
They started drawing right away, and they've been drawing for fun ever since! 
They also enjoy fursuiting in their red panda suit, Blue.</p>

- Twitter: [@Relosaurus](https://twitter.com/Relosaurus)
- FurAffinity: [/relosaurus](https://www.furaffinity.net/user/relosaurus/)
- DeviantArt: [Relosaurus](https://www.deviantart.com/Relosaurus)

</div>
</div>
</div>
</div>
</div>



## 🎧 Featured Musicians

in no particular order: 

* **Alphashock**
  - [facebook](https://www.facebook.com/Alphashock-109154567109659/) / [twitter](https://twitter.com/Alphashock_8)
* **Blaze Shepherd**
  - [twitter](https://twitter.com/blushepherd)
* **n00neimp0rtant**
  - [soundcloud](https://soundcloud.com/n00neimp0rtant-1) / [mixcloud](https://www.mixcloud.com/n00neimp0rtant/) / [twitter](https://twitter.com/n00neimp0rtant/)
* **DJ Workaholic**
  -  [facebook](http://Facebook.com/workaholcRaves) / [twitter](http://Twitter.com/HitchTheFox) / [instagram](http://Instagram.com/dj_workaholic) / [soundcloud](http://Soundcloud.com/dj-workaholic)
* **IPON3**
  - [soundcloud](https://soundcloud.com/dajuanza)
* **Pavlov - Pav-E** 


# Previously featured artists

- LilShark ([@atinylilshark](https://twitter.com/atinylilshark))
- Story ([@story_trail](https://twitter.com/story_trail))
- Short ([@ShortCircuitBat](https://twitter.com/ShortCircuitBat))
- Ryuko ([@RyukoYuzuki](https://twitter.com/RyukoYuzuki))
- Vix ([@VixNdwnq](https://twitter.com/VixNdwnq))
- Sylph ([@tidehardt](https://twitter.com/tidehardt))
- Seb ([@Seb_silverfox](https://twitter.com/Seb_silverfox/))
- ...and many more!
