---
title: Home | WPAFW
permalink: "/"
layout: home
banner: Home
banner-image: hero.jpg
nav: true
nav-title: Home
nav-order: 0
---

<section class="section">

# WPAFW 2023!  Here we Go!

Please stay tuned for more information but WPAFW 2023 will be held on the following dates!:

October 6-8, 2023!
North Park Lodge!

Hotel and registration information coming!





<!-- <div class="buttons">
  [<button class="button is-link">Registration Information</button>](registration)
</div>
</section>
-->
---



<section class="section">

## Contact us

> **Email:** [event@wpafw.org](mailto:event@wpafw.org)
>
> [**North Park Lodge**](https://goo.gl/maps/o1S7uUwtQZ2aN6wi9)
>
> North Ridge Drive
>
> Allison Park, PA 15101

</section>
