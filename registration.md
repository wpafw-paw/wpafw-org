---
title: Registration | WPAFW
permalink: "/registration"
layout: page
banner: 🗳 Registration (2022)
banner-color: blue
nav: true
nav-title: Registration
nav-order: 1
---

## Registration levels

### Attending Membership: ~~$80~~ (At Door: $90)

An attending membership allows a member:
- Access to the event for its duration (Friday, Saturday, Sunday)
- Food and drink at the event
- Any publications at the event
- You also get 2 free seed tickets for the charity auction!

### Sponsor Membership: ~~$100~~ (At Door: $120)

A sponsor gets the full privileges of an attending and much more:
- An awesome Sponsor ribbon
- A special, collectible WPAFW cup
- Expedited badge pickup on site
- An extra 2 seed tickets!
- Access to our Beer and Cider Bar on site (must be over 21)
- Non-alcoholic drinks from local soda company, Natrona Bottling Company! Find more info [here](https://www.natronabottling.com/).

### Super-Sponsor Membership: $200.00

We will lavish you with gifts, praise, and thanks! A Super Sponsor gets the full privileges of a Sponsor membership and much more! Privileges such as:
- An extra special and customized WPAFW gift!
- ANOTHER extra 2 seed tickets!
- A special event (and keepsake) just for Super-Sponsors with the Guests of Honor!

# WPAFW 2022 Pre-Registration

<div class="buttons">
  [<button class="button is-link" disabled>**PRE-REG IS CLOSED!**</button>](#)
</div>

_WPAFW registration is powered by [ConCat](https://concat.app)._

---

## How to pre-register for WPAFW:

1. Go to [our preregistration site](#).
2. Register for an account.
3. Once registered, on the left hand side bar is the option: Register for WPAFW 2021.
4. Select registration level and fill out the required information.
5. After adding your registration to the cart, there is an option to add charity tickets. Click on the link if interested!

If you have any general registration questions prior to the event, please direct them to [registration@wpafw.org](mailto:registration@wpafw.org).

We also take accessibility needs seriously. If you need any accommodations, please be sure to include the information on the registration page! If you have other accessibility related questions, please direct them to [accessibility@wpafw.org](mailto:accessibility@wpafw.org).

---

#### Things To Note

While many cons can offer day passes, the nature of WPAFW (daily food and drink all provided) makes it near impossible to plan for such changes in attendance.  We're sorry for the inconvenience.

For 2022 we are enforcing an 18+ age restriction.

We only accept Visa, MasterCard, Amex, and Discover through ConCat during pre-registration. Cash and card will be accepted at the venue.

## Keep up to date with updates here or the following places:

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-telegram-plane fa-stack-1x fa-inverse"></i>
</span> Telegram Chat](https://t.me/wpafw)

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
</span> Twitter](https://twitter.com/wpafw)

[<span class="fa-stack fa-1x registration-icons">
<i class="fas fa-circle fa-stack-2x"></i>
<i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
</span> Facebook](https://www.facebook.com/wpafw)
