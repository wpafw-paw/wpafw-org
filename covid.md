---
title: COVID-19 Information | WPAFW
permalink: "/covid"
banner: 😷 COVID-19 Information
banner-color: red
nav: false
layout: page
---

> August 28, 2022

## To All our Attendees and Patrons

The board and all of the staff want to thank everyone who has pre-registered and shown support for the event this year!  We also wanted to make sure we get this information out to everyone so that they can finalize plans.

Many of you have questions regarding how WPAFW 2022 will be held with regards to Covid-19 and we’d like to address that:

1. We’re requiring proof of vaccination.  It can be in the form of the physical card, a photo or copy of said card, or a record from an official site like AGH’s MyChart when registering or picking up your badge and swag. However, we will not be requiring masking anywhere at WPAFW.  UPDATE: We will be requiring masks in the shuttles.  We still would like to very much recommend masks in the lodge, especially in the shuttles, and in select places where crowds gather (e.g. the bar).
2. Meal items will be individually packed for safety (buffet style)
3. Shuttles will be sanitized on a regular basis.
4. Microphones for karaoke will be sanitized between use.
5. We will also still provide sanitizer, masks, and gloves throughout the event.

This news comes after much deliberation and discussion.  We fully want to keep people safe and healthy during the event.  We will be monitoring the situation as we get closer to the event, and these rules are subject to change if we feel it’s become a necessary adjustment  

As a reminder, if you have any questions, please feel free to reach out to @wpafw on Twitter or event@wpafw.org or by pinging a mod in the WPAFW Telegram chat.

Thank you again for your time, support, and the overall love you’ve shown to the event.  It means the world to us.

**Justin L. (Manick)
Treasurer of  P.A.W. and Chairman of WPAFW**

**Ben C. (Blithe)
Chairman of P.A.W. and Logistics Director of WPAFW**

**Fisher C. (Oz)
Secretary of P.A.W. and Hospitality Co-Director of WPAFW**

> You can also view this letter [here](https://docs.google.com/document/d/1M2OYH1Z-JDg5iQLMVf1pqploG3A-DL_f/edit?usp=sharing&ouid=110233133296308608367&rtpof=true&sd=true).

---

## Previous announcements

<blockquote class="twitter-tweet">
  <p lang="en" dir="ltr">An update on our Covid restrictions: <a href="https://t.co/0idy1TUTGD">pic.twitter.com/0idy1TUTGD</a></p>&mdash; WPAFW (@wpafw) <a href="https://twitter.com/wpafw/status/1436474276010504196?ref_src=twsrc%5Etfw">September 10, 2021</a>
</blockquote>

<blockquote class="twitter-tweet">
  <p lang="en" dir="ltr">Regarding WPAFW 2021, and how we are planning on moving forward with the event this year: <a href="https://t.co/vBmuhy1Xnw">pic.twitter.com/vBmuhy1Xnw</a></p>&mdash; WPAFW (@wpafw) <a href="https://twitter.com/wpafw/status/1392637718304337921?ref_src=twsrc%5Etfw">May 13, 2021</a>
</blockquote>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
