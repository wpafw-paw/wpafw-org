---
title: Event Policies | WPAFW
permalink: "/policies"
layout: page
banner: 📜 Event Policies
banner-color: purple
nav: true 
nav-title: Policies
nav-order: 8
---

# Event Policies

## General Conduct Policy

The Western Pennsylvania Furry Weekend (WPAFW) is a time where people should be able to relax and enjoy themselves. Common sense and respect for others are usually sufficient to keep everybody happy. However, it is important to note a few rules that we expect everyone to abide by. Be considerate of those around you (especially those who are not in attendance of the convention). Threats, stalking, harassment, or persistently rude behavior may result in your membership being revoked.

During the event, your badge remains the property of WPAFW and must be displayed at all times. If any staff member asks to see it, please present it to them.

These policies must be followed by all attendees, and violations will result in warnings. After 3 warnings, you will lose your WPAFW membership, be asked to leave the event, and may not be welcomed back.

Complaints regarding our attendees from our hotel will be taken seriously, and any actions that may jeopardize our relationship with the hotel may also result in loss of your WPAFW membership.

#### WPAFW cannot permit any violations of local, state, or national law.

We will take severe and immediate action against anyone who brings illegal drugs to the event, engages in vandalism, or blatantly violates any law or acts in a manner that would compromise our relationship with the Community or the Allegheny County Parks Department. Additionally, alcohol not provided and served by WPAFW is prohibited. Anyone found in possession of outside alcohol will be asked to remove it from the event and issued a warning.

#### We do not allow mature subject matter to be displayed in areas where either minors (or adults who do not wish to see it) may be exposed to it.

This includes explicit artwork, but also includes overtly mature discussions, excessive displays of public affection, and use of profanity. While we respect everyone’s lifestyle choices, please remember that North Park is still open to the citizens of Allegheny County during our event. Parents may bring their children to use the playground equipment; especially if the weather is nice.

We expect that all attendees will conduct themselves appropriately. We also expect those staying in the Hotel space to conduct themselves just as they would in any other business or restaurant.

#### Minors are not permitted to attend WPAFW.

No one under 18 will be admitted to the Western PA Furry Weekend.

#### WPAFW expects attendees to dress appropriately.

Costumes and strange clothing are fine, but if you’re showing too much skin or dressed in a manner that is overtly provocative, you will be asked to cover up. Collars and leashes are fine, but overt bondage related behavior is not acceptable in public; so no dragging people around on leashes, please. Additionally, clothing and accessories that intentionally cause distress to other attendees are not welcome. If you’re wearing it, you will be asked to leave.

While collars are fine, leashes pose a safety risk due to the nature of the lodge and line of sight considerations.  Please refrain from using leashes.


#### WPAFW expects attendees who consume beer at the event to do so responsibly.

In accordance with PA state law, if you appear to be visibly intoxicated, our bar wenches WILL cut you off. This decision cannot be appealed to another staff member.

#### Costume is not Consent.

Consent is necessary for all contact between attendees. No one has a right to touch anyone else, hugging or otherwise, without their permission, regardless of whether or not they’re in fursuit.

#### Community Relations

If you have a problem with something involving operations, a WPAFW staff member or another attendee, please address it to Security (Puzz Dragon and his staff) and/or Manick directly.  They will address the concern and will report it to local authorities if deemed necessary.  This does not supersede mandated reporter duties.

If a situation requires urgent care, please inform Security as we have EMS on staff.  

However, in an emergency or criminal situation, do not hesitate to contact the authorities or 911.  Please inform security and/or Manick as soon as possible.

## WPAFW Refund Policy

While we hope that everyone that registers for WPAFW can attend, we understand that situations arise in which it becomes infeasible. Due to this, WPAFW has created the following policies in regards to refunds, including when we can and cannot issue them:

* For the sake of this policy, Sponsorship will be defined as any level of purchase for the event.
* There is a refund deadline every year for Sponsorships which is the last day of Pre-registration`*`. For 2022, that date is **SEPTEMBER 21, 2022, at 11:59 pm EST**. Refund requests after this time will not be granted. Sponsorships cannot be transferred after the deadline.
* In order to obtain a refund, please send an email to registration@wpafw.org. We will need your LEGAL NAME (first and last) and BADGE NAME in order to properly identify you for a refund. NO other method of communication will serve as official in regards to refund requests (i.e. no Twitter, Telegram, etc.).
* Sponsorships will be refunded back to the credit card used in the purchase.
* You may request a transfer of Sponsorship before the refund deadline. We will need the following account information about the transferee: Legal name (first and last), Badge Name, and Date of Birth. Once transferred, the Sponsorship is the property of the person whose name it is in now.
* Sponsorships may be downgraded or upgraded before Pre-registration is closed. In the case of downgrading, a refund will be needed first. It will be open again on-site to upgrade to Sponsor only.

IN THE CASE OF WPAFW BEING CANCELLED: 

* Sponsorships will be refunded back to the purchase point.

`*` For Super-Sponsors this refund deadline is the date of closing for Super-Sponsor registration! For 2022, that date is **AUGUST 22, 2022, 11:59 pm EST**.
