---
title: Events | WPAFW
permalink: "/events"
banner: 🗓️ Events
banner-color: orange
nav: true
nav-title: Events
nav-order: 3
layout: page
---

# Events (2022)

<div class="columns is-mobile is-centered">
<div class="column is-three-quarters">
<iframe src="https://calendar.google.com/calendar/embed?src=c_25e8439991638fe7206a3f6a29aaa1078c027e244e27e34a04bd315881898842%40group.calendar.google.com&ctz=America%2FNew_York&mode=AGENDA" style="border: 0; width: 1024px; height: 1024px" frameborder="0" scrolling="no"></iframe>
</div>
</div>

An iCal compatible version of this calendar is available [here](https://calendar.google.com/calendar/ical/c_25e8439991638fe7206a3f6a29aaa1078c027e244e27e34a04bd315881898842%40group.calendar.google.com/public/basic.ics).

<div class="buttons">
  [<button class="button is-link">Download Events Calendar (PDF)</button>]({{'/assets/WPAFW2022EventSchedule.pdf' | absolute_url}})
</div>

Schedule changes and updates will be posted to the WPAFW Events Telegram Channel @ [t.me/wpafwofficial](https://t.me/wpafwofficial).

We hope to see you there!
