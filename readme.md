---
title: da da da da secret screen | WPAFW
permalink: "/readme"
layout: page
banner: "\U0001F47E da da da da secret screen \U0001F47E"
nav: false
---

# secret readme page for wpafw's website oooo

WPAFW's website runs on Markdown pages processed through
[Jekyll](https://jekyllrb.com/),
and is hosted on
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
It uses
[Bulma](https://bulma.io/)
for layouts, and
[Font Awesome](https://fontawesome.com/)
for icons.

You can find the source code [here](https://github.com/wpafw/wpafw.org).

It was [made by taco](https://daniel.ga/llegos). [( ' v ' )](https://bird.tacowolf.net)
