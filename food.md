---
title: Food | WPAFW
permalink: "/food"
layout: page
banner: 🌮 Food
banner-color: green
nav: true
nav-title: Food
nav-order: 2
---

# Food (2022 Menu)

## Friday Lunch
* Grilled cheese sandwiches
* Tomato soup
* Broccoli/cheese soup
* Mushroom/brown rice soup

## Friday Dinner
* Hamburgers
* Hot dogs
* Vegetarian burgers
* Baked beans
* Cole slaw
* Chips
* Ice cream bars (9pm)

## Saturday Brunch
* Pancakes
* Scrambled eggs
* Sausage
* Vegetarian sausage
* Hash browns
* Oatmeal
* Fruit
* Veggies/dip
* Macaroni salad
* Coffee
* Orange juice

## Saturday Dinner
* Pasta/marinara sauce/meatballs
* Vegetarian balls
* Pasta/alfredo/mixed veggies
* Garlic bread
* Salad

## Sunday Lunch
* Walking tacos
* Clean out the fridge!

## Sunday Dinner
* Domino’s pizza
* Salad
